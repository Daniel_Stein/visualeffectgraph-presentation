﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class ControlVfxTest : MonoBehaviour
{
    VisualEffect visualEffect;
    bool effectIsStopped = false;

    void Start()
    {
        visualEffect = GetComponent<VisualEffect>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (effectIsStopped)
            {
                visualEffect.Play();
            }
            else
            {
                visualEffect.Stop();
            }
            effectIsStopped = !effectIsStopped;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            float force = visualEffect.GetFloat("Attract Force");

            if (force < 6)
            {
                visualEffect.SetFloat("Attract Force", force + 2);
            }
            else
            {
                visualEffect.SetFloat("Attract Force", 0);
            }
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            visualEffect.pause = !visualEffect.pause;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            visualEffect.SendEvent("myPlay");
        }
        visualEffect.GetFloat("Attract Force");
    }
}
