﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] float moveSpeed = 10;
    [SerializeField] float turnSpeed = 5;
    [SerializeField] float maxLookUpAngle = 70;
    [SerializeField] float maxLookDownAngle = 70;

    Vector3 rotation = Vector3.zero;
    bool rotateCamera = true;

    CharacterController charCon;

    // Start is called before the first frame update
    void Start()
    {
        charCon = GetComponent<CharacterController>();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        movement();
        cameraRotation();
    }

    void cameraRotation()
    {
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Q))
        {
            rotateCamera = !rotateCamera;

            if (rotateCamera)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (rotateCamera)
        {
            rotation.x += Input.GetAxis("Mouse Y") * -turnSpeed;
            rotation.y += Input.GetAxis("Mouse X") * turnSpeed;
            if (rotation.x > maxLookDownAngle) rotation.x = maxLookDownAngle;
            if (rotation.x < -maxLookUpAngle) rotation.x = -maxLookUpAngle;
            transform.rotation = Quaternion.Euler(rotation);
        }
    }

    void movement()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            moveSpeed *= 2;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            moveSpeed /= 2;
        }
        
        Vector3 forward = transform.forward;
        forward.y = 0;
        forward.Normalize();

        charCon.SimpleMove(forward * moveSpeed * Input.GetAxis("Vertical"));
        charCon.SimpleMove(transform.TransformDirection(Vector3.right) * moveSpeed * Input.GetAxis("Horizontal"));
    }
}
